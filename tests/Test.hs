module Main (
    main
 ) where

import           Test.HUnit                           hiding (Test)
import           Test.QuickCheck
import           Test.QuickCheck.Property

import           Test.Framework
import           Test.Framework.Providers.HUnit
import           Test.Framework.Providers.QuickCheck2

import           Data.Maybe
import qualified Data.Vector.Unboxed                  as V
import           StrategyF

main :: IO ()
main = defaultMain tests

tests :: [Test]
tests =
  [
    testGroup "X -> O"
    [
        testCase "Not increment in the column in the same direction" test_Case_3,
        testCase "Increment in the column in the same direction" test_Case_4,
        testCase "Not increment in the column in the opposite direction" test_Case_5,
        testCase "Increment in the column in the opposite direction" test_Case_6
    ],

    testGroup "O -> X"
    [
        testCase "Not increment in the column in the same direction" test_Case_7,
        testCase "Increment in the column in the same direction" test_Case_8,
        testCase "Not increment in the column in the opposite direction" test_Case_9,
        testCase "Increment in the column in the opposite direction" test_Case_10
    ],

    testGroup "Properties of the Flow"
    [
            testProperty "The flow has no zeroes" prop_NoZeroes
    ],

    testGroup "Control"
    [
        testCase "Positive Control" test_Case_1,
        testCase "Negative Control" test_Case_2
    ]
  ]

test_Case_1 :: Assertion
test_Case_1 = sqrt 4 @?= 2

test_Case_2 :: Assertion
test_Case_2 = sqrt 4 @?= 3

test_Case_3 :: Assertion
test_Case_3 =
        let
                i = Ebb { unit = 1.0, reference = 3.0, measure = 3 }
                p = 3.9
        in
                updateEbb i p @?= Nothing

test_Case_4 :: Assertion
test_Case_4 =
        let
                i = Ebb { unit = 1.0, reference = 3.0, measure = 3 }
                p = 4.0
        in
                updateEbb i p @?= Just Ebb { unit = 1.0, reference = 4.0, measure = 4 }

test_Case_5 :: Assertion
test_Case_5 =
        let
                i = Ebb { unit = 1.0, reference = 3.0, measure = 3 }
                p = 1.1
        in
                updateEbb i p @?= Nothing

test_Case_6 :: Assertion
test_Case_6 =
        let
                i = Ebb { unit = 1.0, reference = 3.0, measure = 3 }
                p = 1.0
        in
                updateEbb i p @?= Just Ebb { unit = 1.0, reference = 1.0, measure = -1 }

test_Case_7 :: Assertion
test_Case_7 =
        let
                i = Ebb { unit = 1.0, reference = 3.0, measure = -3 }
                p = 2.1
        in
                updateEbb i p @?= Nothing

test_Case_8 :: Assertion
test_Case_8 =
        let
                i = Ebb { unit = 1.0, reference = 3.0, measure = -3 }
                p = 2.0
        in
                updateEbb i p @?= Just Ebb { unit = 1.0, reference = 2.0, measure = -4 }

test_Case_9 :: Assertion
test_Case_9 =
        let
                i = Ebb { unit = 1.0, reference = 3.0, measure = -3 }
                p = 4.0
        in
                updateEbb i p @?= Nothing

test_Case_10 :: Assertion
test_Case_10 =
        let
                i = Ebb { unit = 1.0, reference = 3.0, measure = -3 }
                p = 5.0
        in
                updateEbb i p @?= Just Ebb { unit = 1.0, reference = 5.0, measure = 1 }

instance Arbitrary Ebb where
        arbitrary = do
        	u <- arbitrary
        	r <- arbitrary
        	m <- arbitrary
        	return Ebb { unit = u, reference = r, measure = m }

instance Arbitrary Flow where
        arbitrary = undefined {- do
        	let
        		blank = Flow { unFlow = V.empty }
        		scanMaybe :: (a -> b -> Maybe a) -> a -> [b] -> [a]
        		scanMaybe _ x [] = [x]
        		scanMaybe f x (y:ys) = 
        			let
        				m = f x y
        			in
        				if m == Nothing
        					then 
        	v <- vector 1000
            return Flow { unFlow = scanMaybe f v } -}

prop_NoZeroes :: Flow -> Bool
prop_NoZeroes flow =
        let
                v = unFlow flow
        in
                V.notElem 0 v
