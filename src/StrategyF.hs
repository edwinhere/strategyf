module StrategyF (
        Ebb(Ebb),
        unit,
        reference,
        measure,
        updateEbb,
        Flow(Flow),
        unFlow,
        updateFlow) where

import           Data.Int
import           Data.Maybe
import qualified Data.Vector.Unboxed as V

data Ebb = Ebb {
        unit      :: !Double,
        reference :: !Double,
        measure   :: !Int8
} deriving (Eq, Show)

updateEbb :: Ebb -> Double -> Maybe Ebb
updateEbb ebb input =
        let
                u = unit ebb
                r = reference ebb
                m = measure ebb
                x = truncate $ (input - r) / u
                x' = fromIntegral x
                r' = r + (x' * u)
                m' = if signum x == signum m
                                then m + x
                                else (abs x - 1) * signum x
        in
                if m' /= 0 && m' /= m
                        then Just $ ebb        { reference = r', measure = m' }
                        else Nothing

newtype Flow = Flow { unFlow :: V.Vector Int8 } deriving (Eq, Show)

updateFlow :: Flow -> Ebb -> Maybe Flow
updateFlow flow ebb =
        let
                m = measure ebb
                v = unFlow flow
                sign = signum $ V.last $ v
                new = signum $ m
                appended = V.snoc v m
        in
                if sign /= new
                        then Just $ flow { unFlow = appended }
                        else Nothing
